# import SimpleXMLRPCServer
from xmlrpc.server import SimpleXMLRPCServer 

# import SimpleXMLRPCRequestHandler
from xmlrpc.server import SimpleXMLRPCRequestHandler

#import threading
import threading


# Batasi hanya pada path /RPC2 saja supaya tidak bisa mengakses path lainnya
class RequestHandler(SimpleXMLRPCRequestHandler):
    rpc_paths = ('/RPC2',)

# Buat server
with SimpleXMLRPCServer(('localhost', 8010), 
                        requestHandler=RequestHandler) as server:    
    server.register_introspection_functions()
    # Siapkan lock
    lock = threading.Lock()
    


    # Buat fungsi bernama pinger
    def pinger():
        # Critical section dimulai
        lock.acquire()
        # Membuat x yang berisi ip target
        x = 'www.google.com'
        # Critical section berakhir, harus di unlock
        lock.release()
        # Mereturn x
        return x
        
    # register pinger sebagai ping
    server.register_function(pinger, 'ping')

    print ("Server on going")
    # Jalankan server
    server.serve_forever()
