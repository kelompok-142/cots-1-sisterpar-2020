# a.  Deskripsi Aplikasi
Program ini adalah program untuk melakukan simulasi DDoS, program hanya melakukan ping pada satu alamat dari 1 atau lebih file.
Untuk development programnya kami menggunakan bahasa Pemrograman Python, menggunakan modul XMLRPC server dan client.

# b.  Tugas Anggota
Atazrie Al Hayuma (Developer+Maintainer)<br/>
Rheanov Bija Adzlan (Developer+Maintainer).

# c. Prosedur Instalasi
1.  Jalankan Server<br/>
![](img/serverrun.jpg)<br/>
2.  Jalankan Client 1<br/>
![](img/client1run.jpg)<br/>
3.  Jalankan Client 2<br/>
![](img/client2run.jpg)<br/>
4.  Jalankan Client 3<br/>
![](img/cllient3run.jpg)<br/>

Jika sudah, maka akan seperti ini:
![](img/allrun.jpg)<br/>

# d. Code
Di bawah ini adalah screenshot dari code yang telah dibuat.
1. server <br/>
![](img/server.jpg)<br/>
2. client <br/>
![](img/client.jpg)<br/>

# e. Arsitektur Jaringan
![](img/arsitektur.jpg)<br/>

Ini adalah arsitektur jaringan yang kami gunakan, dimana server memerintah client untuk melakukan DDoS attack pada IP Target yagn sudah ditentukan oleh server
